# README #

The repository contains experimental code. To run the projects, you need to install following:

1. Java 8
2. jBoss Developer Studio (jbdevstudio) 8.0.0GA
3. Google Web Toolkit 2.7.0
4. Google plugin for Eclipse Luna
5. jBoss Wildfly 8 (Application Server)

There is one project I'm working on named "immigration" which contains following projects:

1. ImmigrationEAR (The Enterprise Archive Project)
2. ImmigrationJPA (The Data Model and probably the business logic)
3. ImmigrationWeb (The Web Module with GWT support)