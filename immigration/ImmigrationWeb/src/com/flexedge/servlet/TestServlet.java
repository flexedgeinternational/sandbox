package com.flexedge.servlet;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import com.flexedge.immigration.model.TypeDefinition;
import com.flexedge.service.TypeDefinitionService;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private TypeDefinitionService service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<TypeDefinition> list = service.list();
		for (TypeDefinition def : list) {
			System.err.println(def.getTypeName());
			System.err.println(def.getTypeDefinition());
		}

		String typeName = UUID.randomUUID().toString().substring(0, 32);
		JSONObject definition = new JSONObject();
		definition.put("width", Math.random() * 320);
		definition.put("height", Math.random() * 200);
		service.create(typeName, definition);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
