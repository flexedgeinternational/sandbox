package com.flexedge.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import net.sf.json.JSONObject;

import com.flexedge.immigration.model.TypeDefinition;

/**
 * Session Bean implementation class TypeDefinitionService
 */
@Stateless
@LocalBean
public class TypeDefinitionService {

	@PersistenceContext
	EntityManager em;

	public TypeDefinitionService() {
	}

	public List<TypeDefinition> list() {
		TypedQuery<TypeDefinition> query = em.createNamedQuery("TypeDefinition.findAll", TypeDefinition.class);
		return query.getResultList();
	}

	public void create(String typeName, JSONObject definition) {
		TypeDefinition instance = new TypeDefinition();
		instance.setTypeName(typeName);
		instance.setTypeDefinition(definition);
		em.persist(instance);
	}

}
