package com.flexedge.immigration.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import net.sf.json.JSONObject;

import com.flexedge.immigration.converters.JSONObjectConverter;

/**
 * The persistent class for the type_definition database table.
 * 
 */
@Entity
@Table(name = "type_definition")
@NamedQuery(name = "TypeDefinition.findAll", query = "SELECT t FROM TypeDefinition t")
@Convert(converter = JSONObjectConverter.class, attributeName = "typeDefinition")
public class TypeDefinition implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer oid;
	private JSONObject typeDefinition;
	private String typeName;
	private List<BaseEntity> toBaseEntities;

	public TypeDefinition() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	@Column(name = "type_definition")
	public JSONObject getTypeDefinition() {
		return this.typeDefinition;
	}

	public void setTypeDefinition(JSONObject typeDefinition) {
		this.typeDefinition = typeDefinition;
	}

	@Column(name = "type_name")
	public String getTypeName() {
		return this.typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	// bi-directional many-to-one association to BaseEntity
	@OneToMany(mappedBy = "toTypeDefinition", cascade = { CascadeType.ALL })
	public List<BaseEntity> getToBaseEntities() {
		return this.toBaseEntities;
	}

	public void setToBaseEntities(List<BaseEntity> toBaseEntities) {
		this.toBaseEntities = toBaseEntities;
	}

	public BaseEntity addToBaseEntity(BaseEntity toBaseEntity) {
		getToBaseEntities().add(toBaseEntity);
		toBaseEntity.setToTypeDefinition(this);

		return toBaseEntity;
	}

	public BaseEntity removeToBaseEntity(BaseEntity toBaseEntity) {
		getToBaseEntities().remove(toBaseEntity);
		toBaseEntity.setToTypeDefinition(null);

		return toBaseEntity;
	}

}