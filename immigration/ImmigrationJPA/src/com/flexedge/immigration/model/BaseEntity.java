package com.flexedge.immigration.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import net.sf.json.JSONObject;

import com.flexedge.immigration.converters.JSONObjectConverter;

/**
 * The persistent class for the base_entity database table.
 * 
 */
@Entity
@Table(name = "base_entity")
@NamedQuery(name = "BaseEntity.findAll", query = "SELECT b FROM BaseEntity b")
@Convert(converter = JSONObjectConverter.class, attributeName = "typeData")
public class BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer oid;
	private String createdBy;
	private Timestamp dateCreated;
	private Timestamp dateModified;
	private String modifiedBy;
	private JSONObject typeData;
	private TypeDefinition toTypeDefinition;

	public BaseEntity() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "date_created")
	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Column(name = "date_modified")
	public Timestamp getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Timestamp dateModified) {
		this.dateModified = dateModified;
	}

	@Column(name = "modified_by")
	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "type_data")
	public JSONObject getTypeData() {
		return this.typeData;
	}

	public void setTypeData(JSONObject typeData) {
		this.typeData = typeData;
	}

	// bi-directional many-to-one association to TypeDefinition
	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@JoinColumn(name = "oid_type_definition")
	public TypeDefinition getToTypeDefinition() {
		return this.toTypeDefinition;
	}

	public void setToTypeDefinition(TypeDefinition toTypeDefinition) {
		this.toTypeDefinition = toTypeDefinition;
	}

}