package com.flexedge.immigration.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import net.sf.json.JSONObject;

@Converter(autoApply = true)
public class JSONObjectConverter implements AttributeConverter<JSONObject, String> {

	@Override
	public String convertToDatabaseColumn(JSONObject value) {
		return value != null ? value.toString() : null;
	}

	@Override
	public JSONObject convertToEntityAttribute(String value) {
		return value != null ? JSONObject.fromObject(value) : null;
	}

}
